<?php
/**
 * Sets our SCSS constant for the improved build process
 *
 * @since 2.7.0
 */
if ( !defined( 'HJI_BLVD_SCSS' ) ) {
    define( 'HJI_BLVD_SCSS', true );
}

if ( !function_exists( 'hji_new_widgets' ) ) :
/**
 * Override main theme widget areas
 */
function hji_new_widgets() {
  // Sidebars

  register_sidebar( array(
    'id'            => 'blvd-upper-homewidgets',
    'name'          => __( 'Homepage Upper Widgets', 'hji-textdomain' ),
    'description'   => __( 'Homepage Upper Widgets' ),
    'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
    'after_widget'  => "</div>",
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ));

    register_sidebar( array(
    'id'            => 'blvd-bottom-homewidgets',
    'name'          => __( 'Homepage Bottom Widgets', 'hji-textdomain' ),
    'description'   => __( 'Homepage Bottom Widgets' ),
    'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
    'after_widget'  => "</div>",
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ));

    register_sidebar( array(
    'id'            => 'blvd-homepageparallax',
    'name'          => __( 'Homepage Parallax', 'hji-textdomain' ),
    'description'   => __( 'Homepage Parallax Image' ),
    'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
    'after_widget'  => "</div>",
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ));

}
add_action( 'widgets_init', 'hji_new_widgets' );
endif;



/**
 * Change label of Keyword/MLS field
 * 
 */
function remove_mls_label( $vars ) {
    $vars['label'] = "Keyword";

    return $vars;
}
add_filter( 'ridx_search_form_field_keyword', 'remove_mls_label' );